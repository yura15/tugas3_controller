@extends('master.app')

@section('navigasi')

    <!-- Page Header Start -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Pendidikan Yura Regita !</h2>
                </div>
                <div class="col-12">
                    <a href="">Pendidikan</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Team Start -->
    <div class="team">
        <div class="container">
            <div class="section-header text-center">
                <p>Chek this !</p>
                <h2>Pendidikan yang sudah dan masih ditempuh..</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="team-item">
                        <div class="team-img">
                            <img src="img/team-1.jpg" alt="Team Image">
                        </div>
                        <div class="team-text">
                            <h2>TK Widya Dharma</h2>
                            <p>2006 - 2007</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="team-item">
                        <div class="team-img">
                            <img src="img/team-2.jpg" alt="Team Image">
                        </div>
                        <div class="team-text">
                            <h2>SD Negeri 1 Temukus</h2>
                            <p>2007 - 2013</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="team-item">
                        <div class="team-img">
                            <img src="img/team-3.jpg" alt="Team Image">
                        </div>
                        <div class="team-text">
                            <h2>SMP Negeri 1 Seririt</h2>
                            <p>2013 - 2016</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="team-item">
                        <div class="team-img">
                            <img src="img/team-4.jpg" alt="Team Image">
                        </div>
                        <div class="team-text">
                            <h2>SMK N Bali Mandara</h2>
                            <p>2016 - 2019</p>
                        </div>
                    </div>
                </div>
                <center>
                <div class="col-lg-3 col-md-6">
                    <div class="team-item">
                        <div class="team-img">
                            <img src="img/team-5.jpg" alt="Team Image">
                        </div>
                        <div class="team-text">
                            <h2>Universitas Pendidikan Ganesha</h2>
                            <p>2019 - 2023</p>
                        </div>
                    </div>
                </div>
                </center>
            </div>
        </div>
    </div>
    <!-- Team End -->

@endsection     