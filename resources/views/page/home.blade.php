@extends('master.app')

@section('navigasi')
    <!-- Hero Start -->
    <div class="hero">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="hero-text">
                        <h1>Hi All, This is me Yura Regita !</h1>
                        <p>
                           Ingin tahu lebih mengenai saya, silahkan kunjungi Personal Profile ini. Enjoy guys ! 
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 d-none d-md-block">
                    <div class="hero-image">
                        <img src="img/hero.png" alt="Hero Image">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero End -->
@endsection
