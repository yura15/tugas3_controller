<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Yura Personal Profile</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free Website Template" name="keywords">
        <meta content="Free Website Template" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">

        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>

    @if ($page==1)              
    <body class="home">
        @elseif($page==2)
        <body class="about">
            @elseif($page==3)
            <body class="portofolio">
                @elseif($page==4)
                <body class="pendidikan">
                    @elseif($page==5) 
                    <body class="contact">   
    @endif

    <body>
        <!-- Top Bar Start -->
        <div class="top-bar d-none d-md-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="top-bar-left">
                            <div class="text">
                                <h2>Hi !</h2>
                                <p>This is Me !</p>
                            </div>
                            <div class="text">
                                <h2>081 805 550 160</h2>
                                <p>Call me if you need !</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="top-bar-right">
                            <div class="social">
                                <a href="https://twitter.com/RegitaYura"><i class="fab fa-twitter"></i></a>
                                <a href="https://www.facebook.com/yuraregitha/"><i class="fab fa-facebook"></i></a>
                                <a href="https://www.instagram.com/yuraregita/"><i class="fab fa-instagram"></i></a>
                                <a href="https://www.youtube.com/channel/UCz_90ZY58Hb13fcG1CUcMSw"><i class="fab fa-youtube"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Bar End -->

        <!-- Nav Bar Start -->
        <div class="navbar navbar-expand-lg bg-dark navbar-dark">
            <div class="container-fluid">
                <a href="/" class="navbar-brand">Personal <span>Profile</span></a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav ml-auto">
                        @if ($page==1)
                        <li class="nav-item active bg-dark">
                            @else
                            <li class="nav-item">
                            @endif
                            <a href="{{route('home')}}" class="nav-link">Home</a>
                        </li>
                        @if ($page==2)
                        <li class="nav-item active bg-dark">
                            @else
                            <li class="nav-item">
                            @endif
                            <a href="{{route('about')}}" class="nav-link">About</a>
                        </li>
                        @if ($page==3)
                        <li class="nav-item active bg-dark">
                            @else
                            <li class="nav-item">
                            @endif
                            <a href="{{route('portofolio')}}" class="nav-link">Portofolio</a>
                        </li>
                        @if ($page==4)
                        <li class="nav-item active bg-dark">
                            @else
                            <li class="nav-item">
                            @endif
                            <a href="{{route('pendidikan')}}" class="nav-link">Pendidikan</a>
                        </li>
                        @if ($page==5)
                        <li class="nav-item active bg-dark">
                            @else
                            <li class="nav-item">
                            @endif
                            <a href="{{route('contact')}}" class="nav-link">Contact</a>
                        </li>
                    </div>
                </div>
            </div>
        </div>
        <!-- Nav Bar End -->

        @yield('navigasi')

        <!-- Footer Start -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="footer-contact">
                                    <h2>Yura Regita's Address</h2>
                                    <p><i class="fa fa-map-marker-alt"></i>Dusun Pegayaman, Desa Temukus</p>
                                    <p><i class="fa fa-phone-alt"></i>081 805 550 160</p>
                                    <p><i class="fa fa-envelope"></i>yuraregitha17@gmail.com</p>
                                    <div class="footer-social">
                                        <a href="https://twitter.com/RegitaYura"><i class="fab fa-twitter"></i></a>
                                        <a href="https://www.facebook.com/yuraregitha/"><i class="fab fa-facebook"></i></a>
                                        <a href="https://www.instagram.com/yuraregita/"><i class="fab fa-instagram"></i></a>
                                        <a href="https://www.youtube.com/channel/UCz_90ZY58Hb13fcG1CUcMSw"><i class="fab fa-youtube"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="footer-newsletter">
                            <h2>Newsletter</h2>
                            <p>
                                Masukan Email Anda Disini !
                            </p>
                            <div class="form">
                                <input class="form-control" placeholder="Email goes here">
                                <button class="btn">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container copyright">
                <div class="row">
                    <div class="col-md-5">
                        <p>&copy; <a href="#">Yura Regita</a>, All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>
        <script src="lib/lightbox/js/lightbox.min.js"></script>
        
        <!-- Contact Javascript File -->
        <script src="mail/jqBootstrapValidation.min.js"></script>
        <script src="mail/contact.js"></script>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>