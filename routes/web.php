<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[App\Http\Controllers\HomeController::class,'index'])->name('home');
Route::get('home-saya',[App\Http\Controllers\HomeController::class,'index'])->name('home');
Route::get('about-saya',[App\Http\Controllers\AboutController::class,'index'])->name('about');
Route::get('portofolio-saya',[App\Http\Controllers\PortofolioController::class,'index'])->name('portofolio');
Route::get('pendidikan-saya',[App\Http\Controllers\PendidikanController::class,'index'])->name('pendidikan');
Route::get('contact-saya',[App\Http\Controllers\ContactController::class,'index'])->name('contact');

// Route::get('/', function () {
//     $page = 1;
//     return view('page.home', compact('page'));
// })->name('home');

// Route::get('/home', function () {
//     $page = 1;
//     return view('page.home', compact('page'));
// })->name('home');

// Route::get('/about', function () {
//     $page = 2;
//     return view('page.about', compact('page'));
// })->name('about');

// Route::get('/portofolio', function () {
//     $page = 3;
//     return view('page.portofolio', compact('page'));
// })->name('portofolio');

// Route::get('/pendidikan', function () {
//     $page = 4;
//     return view('page.pendidikan', compact('page'));
// })->name('pendidikan');

// Route::get('/contact', function () {
//     $page = 5;
//     return view('page.contact', compact('page'));
// })->name('contact');